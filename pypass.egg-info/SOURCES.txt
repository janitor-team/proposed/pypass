.travis.yml
AUTHORS
ChangeLog
Dockerfile
LICENSE
Makefile
README.rst
docs-requirements.txt
requirements.txt
setup.cfg
setup.py
test-requirements.txt
tox.ini
completion/pypass.bash-completion
docs/source/API.rst
docs/source/conf.py
docs/source/index.rst
docs/source/introduction.rst
docs/source/manpage.rst
pypass/__init__.py
pypass/command.py
pypass/entry_type.py
pypass/passwordstore.py
pypass.egg-info/PKG-INFO
pypass.egg-info/SOURCES.txt
pypass.egg-info/dependency_links.txt
pypass.egg-info/entry_points.txt
pypass.egg-info/not-zip-safe
pypass.egg-info/pbr.json
pypass.egg-info/requires.txt
pypass.egg-info/top_level.txt
pypass/tests/__init__.py
pypass/tests/test_command.py
pypass/tests/test_key_sec.asc
pypass/tests/test_ownertrust.txt
pypass/tests/test_passwordstore.py